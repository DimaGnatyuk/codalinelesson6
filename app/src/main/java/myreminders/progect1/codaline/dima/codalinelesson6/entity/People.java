package myreminders.progect1.codaline.dima.codalinelesson6.entity;

/**
 * Created by Admin on 19.03.2016.
 */
public class People {
    private String name = "";
    private String uri_image = "";

    public People (String name, String uri_image)
    {
        this.name=name;
        this.uri_image=uri_image;
    }

    public String getUri_image() {
        return uri_image;
    }

    public void setUri_image(String uri_image) {
        this.uri_image = uri_image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

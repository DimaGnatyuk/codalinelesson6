package myreminders.progect1.codaline.dima.codalinelesson6;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import myreminders.progect1.codaline.dima.codalinelesson6.adapter.CustomAdapter;
import myreminders.progect1.codaline.dima.codalinelesson6.entity.People;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private final String respance = "";
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.TextView);
    }

    @Override
     protected void onStart() {
        super.onStart();
        MyLoadTask task = new MyLoadTask(textView);
        task.execute("https://api.vk.com/method/users.get?user_ids=156168343&fields=photo_100");
        //new AsyncTask
    }
}

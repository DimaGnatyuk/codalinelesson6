package myreminders.progect1.codaline.dima.codalinelesson6.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import myreminders.progect1.codaline.dima.codalinelesson6.R;
import myreminders.progect1.codaline.dima.codalinelesson6.entity.People;

public class CustomAdapter extends ArrayAdapter<People> {

    public CustomAdapter(Context context, List<People> objects) {
        super(context, R.layout.fragment_people, objects);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.fragment_people, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        People people = getItem(position);
        holder.textViewName.setText(String.format(people.getName()));
        //holder.imageView.setImageBitmap(String.format(people.getName()));

        return convertView;
    }

    private class Holder {
        TextView textViewName;
        ImageView imageView;

        public Holder(View rootView) {
            this.textViewName = (TextView) rootView.findViewById(R.id.TextView);
            this.imageView = (ImageView) rootView.findViewById(R.id.ImageView);
        }
    }
}

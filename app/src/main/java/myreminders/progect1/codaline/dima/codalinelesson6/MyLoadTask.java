package myreminders.progect1.codaline.dima.codalinelesson6;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import myreminders.progect1.codaline.dima.codalinelesson6.adapter.CustomAdapter;
import myreminders.progect1.codaline.dima.codalinelesson6.entity.People;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Admin on 19.03.2016.
 */
public class MyLoadTask extends AsyncTask<String, Void, Boolean> {
    private static final String TAG = MyLoadTask.class.getSimpleName();

    private OkHttpClient mOkHttpClient = new OkHttpClient();
    private String mLoad;
    private TextView textView;

    public MyLoadTask(TextView textView) {
        this.textView = textView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        /*mProgressBar.setVisibility(View.VISIBLE);
        mTextView.setText("");*/
    }

    @Override
    protected Boolean doInBackground(String... params) {

        //imitate hard work



        //request
        Request request = new Request.Builder()
                .url(params[0])
                .build();
        try {
            Response response = mOkHttpClient.newCall(request).execute();
            int code = response.code();
            Log.d(TAG, "The response is: " + code);

            mLoad = response.body().string();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        textView.setText(mLoad.toString());
        //respance = mLoad;
        /*
        mProgressBar.setVisibility(View.GONE);*/
    }

}
